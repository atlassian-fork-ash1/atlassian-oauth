# OAuth Plugin

## Description

Atlassian applications house a large quantity of data that is used in the day to day lives of developers, managers and other stakeholders involved in developing software. Right now, the only place they can access that data is from within Atlassian applications. To allow them to access it from 3rd party sites, such as iGoogle or GMail, they need to be able to authorize these 3rd parties to access their data on their behalf. For some of these users, Atlassian products like JIRA are their main destination on the web. For JIRA to be able to pull information from 3rd party applications, JIRA and it's plugins need a way to request access and gain authorization to use the users data from 3rd party applications. OAuth provides a means of allowing the user to delegate authorization to access their data. Our goal is to allow Atlassian applications to act as both Service Providers and Consumers.

## Atlassian Developer?

### Committing Guidelines

Please see [The Platform Rules of Engagement (go/proe)](http://go.atlassian.com/proe) for committing to this module.
The main active branches are:

- `atlassian-oauth-2.0.x`: Bugfixes for the next backwards compatible release of Atlassian OAuth. Topic branches here should be prefixed with issue-2.0/.
- `master`: New features changes working towards oauth 2.1.x Topic branches here should be prefixed with issue/.

### Builds

The Bamboo builds for this project are on [EcoBAC](https://ecosystem-bamboo.internal.atlassian.com/browse/OAUTH)

## External User?

### Issues

Please raise any issues you find with this module in [JIRA](https://ecosystem.atlassian.net/browse/OAUTH)

### Documentation

[OAuth Documentation](https://ecosystem.atlassian.net/wiki/display/OAUTH)
