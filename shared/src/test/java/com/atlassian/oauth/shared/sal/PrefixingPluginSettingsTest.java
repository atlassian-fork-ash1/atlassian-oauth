package com.atlassian.oauth.shared.sal;

import com.atlassian.sal.api.pluginsettings.PluginSettings;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class PrefixingPluginSettingsTest {
    private static final String PREFIX = "prefix";

    @Mock
    PluginSettings pluginSettings;

    PluginSettings prefixingPluginSettings;

    @Before
    public void setUp() {
        prefixingPluginSettings = new PrefixingPluginSettings(pluginSettings, PREFIX);
    }

    @Test
    public void verifyThatGetCallsBackingPluginSettingsWithPrefixedKey() {
        prefixingPluginSettings.get("key");

        verify(pluginSettings).get(PREFIX + ".key");
    }

    @Test
    public void verifyThatPutCallsBackingPluginSettingsWithPrefixedKey() {
        prefixingPluginSettings.put("key", "value");

        verify(pluginSettings).put(PREFIX + ".key", "value");
    }

    @Test
    public void verifyThatRemoveCallsBackingPluginSettingsWithPrefixedKey() {
        prefixingPluginSettings.remove("key");

        verify(pluginSettings).remove(PREFIX + ".key");
    }
}
