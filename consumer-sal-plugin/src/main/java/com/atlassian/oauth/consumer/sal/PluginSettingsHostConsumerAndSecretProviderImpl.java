package com.atlassian.oauth.consumer.sal;

import com.atlassian.oauth.Consumer;
import com.atlassian.oauth.consumer.ConsumerCreationException;
import com.atlassian.oauth.consumer.ConsumerService;
import com.atlassian.oauth.consumer.core.ConsumerServiceStore.ConsumerAndSecret;
import com.atlassian.oauth.consumer.core.HostConsumerAndSecretProvider;
import com.atlassian.oauth.shared.sal.PrefixingPluginSettings;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;

import java.security.GeneralSecurityException;
import java.security.KeyPair;
import java.util.Properties;
import java.util.Random;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * This is a simple implementation of the {@link HostConsumerAndSecretProvider}.  It uses {@link PluginSettings} to
 * store the consumer information.  When determining if it should generate the host applications consumer information,
 * it checks for the existence of the consumer information in the {@code PluginSettings}.  If found, it returns it.
 * If it's not found, it generates new values and stores them.
 *
 * <p>Note: Due to the use of {@code PluginSettings} and lack of peer coordination, it is not recommended that this
 * implementation is used in a clustered environment.  Applications that may be clustered should create their own
 * implementation of the {@code HostConsumerAndSecretProvider} interface.
 */
public class PluginSettingsHostConsumerAndSecretProviderImpl implements HostConsumerAndSecretProvider {
    static final String HOST_SERVICENAME = "__HOST_SERVICE__";

    private final ApplicationProperties applicationProperties;
    private final KeyPairFactory keyPairFactory;
    private final PluginSettingsFactory pluginSettingsFactory;
    private final I18nResolver i18n;

    public PluginSettingsHostConsumerAndSecretProviderImpl(ApplicationProperties applicationProperties,
                                                           PluginSettingsFactory pluginSettingsFactory,
                                                           KeyPairFactory keyPairFactory,
                                                           I18nResolver i18nResolver) {
        this.applicationProperties = checkNotNull(applicationProperties, "applicationProperties");
        this.pluginSettingsFactory = checkNotNull(pluginSettingsFactory, "pluginSettingsFactory");
        this.keyPairFactory = checkNotNull(keyPairFactory, "keyPairFactory");
        this.i18n = checkNotNull(i18nResolver, "i18nResolver");
    }

    public synchronized ConsumerAndSecret get() {
        Settings settings = settings();
        ConsumerAndSecret hostCas = settings.get();
        if (hostCas != null) {
            return hostCas;
        }

        hostCas = createHostConsumerAndSecret();
        settings.put(hostCas);
        return hostCas;
    }

    public ConsumerAndSecret put(ConsumerAndSecret hostCas) {
        Settings settings = settings();
        settings.put(hostCas);
        return settings.get();
    }

    private ConsumerAndSecret createHostConsumerAndSecret() {
        String key = generateConsumerKey();
        KeyPair keyPair;
        try {
            keyPair = keyPairFactory.newKeyPair();
        } catch (GeneralSecurityException e) {
            throw new ConsumerCreationException("Could not create key pair for consumer", e);
        }

        Consumer consumer = Consumer.key(key)
                .name(applicationProperties.getDisplayName())
                .publicKey(keyPair.getPublic())
                .description(
                        i18n.getText("host.consumer.default.description",
                                applicationProperties.getDisplayName(),
                                applicationProperties.getBaseUrl()
                        )
                )
                .build();
        return new ConsumerAndSecret(HOST_SERVICENAME, consumer, keyPair.getPrivate());
    }

    private String generateConsumerKey() {
        String key = applicationProperties.getDisplayName() + ":";
        Random random = new Random();
        for (int i = 0; i < 10; i++) {
            key += random.nextInt(10);
        }
        return key;
    }

    private Settings settings() {
        return new Settings(pluginSettingsFactory.createGlobalSettings());
    }

    final class Settings {
        private PluginSettings settings;

        public Settings(PluginSettings settings) {
            this.settings = new PrefixingPluginSettings(
                    checkNotNull(settings, "settings"), ConsumerService.class.getName() + ":host");
        }

        public ConsumerAndSecret get() {
            Properties props = (Properties) settings.get(HOST_SERVICENAME);
            if (props == null) {
                return null;
            }
            return new ConsumerProperties(props).asConsumerAndSecret(HOST_SERVICENAME);
        }

        public void put(ConsumerAndSecret cas) {
            settings.put(HOST_SERVICENAME, new ConsumerProperties(cas).asProperties());
        }
    }
}
