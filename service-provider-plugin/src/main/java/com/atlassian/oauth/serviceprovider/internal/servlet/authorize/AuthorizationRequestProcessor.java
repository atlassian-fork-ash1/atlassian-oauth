package com.atlassian.oauth.serviceprovider.internal.servlet.authorize;

import com.atlassian.oauth.serviceprovider.ServiceProviderToken;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

interface AuthorizationRequestProcessor {
    void process(HttpServletRequest request, HttpServletResponse response, ServiceProviderToken token) throws IOException;
}