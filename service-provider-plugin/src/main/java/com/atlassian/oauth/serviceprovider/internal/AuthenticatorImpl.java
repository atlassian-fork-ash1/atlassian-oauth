package com.atlassian.oauth.serviceprovider.internal;

import com.atlassian.oauth.Consumer;
import com.atlassian.oauth.serviceprovider.Clock;
import com.atlassian.oauth.serviceprovider.InvalidTokenException;
import com.atlassian.oauth.serviceprovider.ServiceProviderConsumerStore;
import com.atlassian.oauth.serviceprovider.ServiceProviderToken;
import com.atlassian.oauth.serviceprovider.ServiceProviderTokenStore;
import com.atlassian.oauth.serviceprovider.internal.OAuthProblem.Problem;
import com.atlassian.oauth.serviceprovider.internal.servlet.OAuthRequestUtils;
import com.atlassian.oauth.util.RequestAnnotations;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.auth.AuthenticationController;
import com.atlassian.sal.api.auth.Authenticator;
import com.atlassian.sal.api.transaction.TransactionCallback;
import com.atlassian.sal.api.transaction.TransactionTemplate;
import com.atlassian.sal.api.user.UserManager;
import net.oauth.OAuthAccessor;
import net.oauth.OAuthConsumer;
import net.oauth.OAuthException;
import net.oauth.OAuthMessage;
import net.oauth.OAuthProblemException;
import net.oauth.OAuthValidator;
import net.oauth.server.OAuthServlet;
import net.oauth.signature.RSA_SHA1;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.security.Principal;
import java.util.Locale;
import java.util.Map;

import static com.atlassian.oauth.serviceprovider.internal.servlet.OAuthProblemUtils.logOAuthProblem;
import static com.google.common.base.Preconditions.checkNotNull;
import static net.oauth.OAuth.Problems.CONSUMER_KEY_UNKNOWN;
import static net.oauth.OAuth.Problems.PERMISSION_DENIED;
import static net.oauth.OAuth.Problems.TOKEN_EXPIRED;
import static net.oauth.OAuth.Problems.TOKEN_REJECTED;

public class AuthenticatorImpl implements Authenticator {
    private static final Logger LOG = LoggerFactory.getLogger(AuthenticatorImpl.class);

    /**
     * The request attribute key that the request dispatcher uses to store the
     * original URL for a forwarded request.
     */
    private static final String FORWARD_REQUEST_URI = "javax.servlet.forward.request_uri";

    /**
     * requestor id parameter for 2LO impersonation.
     */
    private static final String XOAUTH_REQUESTOR_ID = "xoauth_requestor_id";

    /**
     * OAUTH-262, OAUTH-263: for backward compat with remoteapps 2LO
     * deprecated requestor id parameter for 2LO impersonation.
     */
    @Deprecated
    private static final String REMOTEAPP_REQUESTOR_ID = "user_id";

    private final ServiceProviderTokenStore store;
    private final OAuthValidator validator;
    private final OAuthConverter converter;
    private final AuthenticationController authenticationController;
    private final TransactionTemplate transactionTemplate;
    private final ApplicationProperties applicationProperties;
    private final Clock clock;
    private final ServiceProviderConsumerStore serviceProviderConsumerStore;
    private final UserManager userManager;

    public AuthenticatorImpl(@Qualifier("tokenStore") ServiceProviderTokenStore store,
                             OAuthValidator validator,
                             OAuthConverter converter,
                             AuthenticationController authenticationController,
                             TransactionTemplate transactionTemplate,
                             ApplicationProperties applicationProperties,
                             Clock clock,
                             ServiceProviderConsumerStore serviceProviderConsumerStore,
                             UserManager userManager) {
        this.store = checkNotNull(store, "store");
        this.validator = checkNotNull(validator, "validator");
        this.converter = checkNotNull(converter, "converter");
        this.authenticationController = checkNotNull(authenticationController, "authenticationController");
        this.transactionTemplate = checkNotNull(transactionTemplate, "transactionTemplate");
        this.applicationProperties = checkNotNull(applicationProperties, "applicationProperties");
        this.clock = checkNotNull(clock, "clock");
        this.serviceProviderConsumerStore = checkNotNull(serviceProviderConsumerStore, "serviceProviderConsumerStore");
        this.userManager = checkNotNull(userManager, "userManager");
    }

    public Authenticator.Result authenticate(HttpServletRequest request, HttpServletResponse response) {
        if (OAuthRequestUtils.is2LOAuthAccessAttempt(request)) {
            return authenticate2LORequest(request, response);
        } else if (OAuthRequestUtils.is3LOAuthAccessAttempt(request)) {
            return authenticate3LORequest(request, response);
        } else {
            throw new IllegalArgumentException("This Authenticator only works with OAuth requests");
        }
    }

    private Consumer validateConsumer(OAuthMessage message) throws IOException, OAuthException {
        // This consumer must exist at the time the token is used.
        final String consumerKey = message.getConsumerKey();
        final Consumer consumer = serviceProviderConsumerStore.get(consumerKey);

        if (consumer == null) {
            LOG.info("Unknown consumer key:'{}' supplied in OAuth request" + consumerKey);
            throw new OAuthProblemException(CONSUMER_KEY_UNKNOWN);
        }

        return consumer;
    }

    void validate2LOMessage(OAuthMessage message, Consumer consumer)
            throws OAuthException, IOException, URISyntaxException {
        final OAuthConsumer oauthConsumer = converter.toOAuthConsumer(consumer);
        oauthConsumer.setProperty(RSA_SHA1.PUBLIC_KEY, consumer.getPublicKey().getEncoded());
        final OAuthAccessor oauthAccessor = new OAuthAccessor(oauthConsumer);

        printMessageToDebug(message);

        validator.validateMessage(message, oauthAccessor);
    }

    private void printMessageToDebug(OAuthMessage message) throws IOException {
        if (!LOG.isDebugEnabled()) {
            return;
        }

        StringBuilder sb = new StringBuilder("Validating incoming OAuth request:\n");
        sb.append("\turl: ").append(message.URL).append("\n");
        sb.append("\tmethod: ").append(message.method).append("\n");
        for (Map.Entry<String, String> entry : message.getParameters()) {
            sb.append("\t").append(entry.getKey()).append(": ").append(entry.getValue()).append("\n");
        }
        LOG.debug(sb.toString());
    }

    void validate3LOMessage(OAuthMessage message, ServiceProviderToken token)
            throws OAuthException, IOException, URISyntaxException {
        printMessageToDebug(message);

        validator.validateMessage(message, converter.toOAuthAccessor(token));
    }

    public Authenticator.Result authenticate3LORequest(HttpServletRequest request, HttpServletResponse response) {
        OAuthMessage message = OAuthServlet.getMessage(request, getLogicalUri(request));

        // 3LO needs to start with oauth_token
        String tokenStr;
        Consumer consumer;
        try {
            tokenStr = message.getToken();
        } catch (IOException e) {
            // this would be really strange if it happened, but take precautions just in case
            LOG.error("3-Legged-OAuth Failed to read token from request", e);
            sendError(response, HttpServletResponse.SC_INTERNAL_SERVER_ERROR, message);
            return new Result.Error(new OAuthProblem.UnreadableToken(e));
        }

        ServiceProviderToken token;
        try {
            try {
                // the oauth_token must exist and it has to be valid
                token = getToken(tokenStr);
            } catch (InvalidTokenException e) {
                LOG.debug(String.format("3-Legged-OAuth Consumer provided token [%s] rejected by ServiceProviderTokenStore", tokenStr), e);
                throw new OAuthProblemException(TOKEN_REJECTED);
            }

            // various validations on the token
            if (token == null) {
                if (LOG.isDebugEnabled()) {
                    LOG.debug(String.format("3-Legged-OAuth token rejected. Service Provider Token, for Consumer provided token [%s], is null", tokenStr));
                }

                throw new OAuthProblemException(TOKEN_REJECTED);
            }

            if (!token.isAccessToken()) {
                if (LOG.isDebugEnabled()) {
                    LOG.debug(String.format("3-Legged-OAuth token rejected. Service Provider Token, for Consumer provided token [%s], is NOT an access token.", tokenStr));
                }

                throw new OAuthProblemException(TOKEN_REJECTED);
            }

            if (!token.getConsumer().getKey().equals(message.getConsumerKey())) {
                if (LOG.isDebugEnabled()) {
                    LOG.debug(String.format("3-Legged-OAuth token rejected. Service Provider Token, for Consumer provided token [%s], consumer key [%s] does not match request consumer key [%s]", tokenStr, token.getConsumer().getKey(), message.getConsumerKey()));
                }

                throw new OAuthProblemException(TOKEN_REJECTED);
            }

            if (token.hasExpired(clock)) {
                if (LOG.isDebugEnabled()) {
                    LOG.debug(String.format("3-Legged-OAuth token rejected. Token has expired. Token creation time [%d] time to live [%d] clock (contains logging delay) [%d]", token.getCreationTime(), token.getTimeToLive(), clock.timeInMilliseconds()));
                }

                throw new OAuthProblemException(TOKEN_EXPIRED);
            }
            validate3LOMessage(message, token);
            consumer = validateConsumer(message);

            if (!consumer.getThreeLOAllowed()) {
                LOG.info("3-Legged-OAuth request has been attempted but 3-Legged-OAuth is not enabled for consumer:'{}'." + consumer.getKey());
                throw new OAuthProblemException(PERMISSION_DENIED);
            }
        } catch (OAuthProblemException ope) {
            return handleOAuthProblemException(response, message, tokenStr, ope);
        } catch (Exception e) {
            return handleException(response, message, e);
        }

        final Principal user = token.getUser();

        RequestAnnotations.setOAuthConsumerKey(request, consumer.getKey());
        LOG.debug(String.format("3-Legged-OAuth successful. Request marked with consumer key set to [%s]", consumer.getKey()));

        return getUserLoginResult(request, response, message, consumer, user);
    }

    public Authenticator.Result authenticate2LORequest(HttpServletRequest request, HttpServletResponse response) {
        OAuthMessage message = OAuthServlet.getMessage(request, getLogicalUri(request));

        Consumer consumer;
        try {
            consumer = validateConsumer(message);
            validate2LOMessage(message, consumer);
        } catch (OAuthProblemException ope) {
            return handleOAuthProblemException(response, message, null, ope);
        } catch (Exception e) {
            return handleException(response, message, e);
        }

        Principal user;
        String userId = request.getParameter(XOAUTH_REQUESTOR_ID);
        LOG.debug("2-Legged-OAuth userId [{}] from request parameter [{}].", userId, XOAUTH_REQUESTOR_ID);

        // OAUTH-262, OAUTH-263: for backward compat with remoteapps 2LO
        // if {@link #XOAUTH_REQUESTOR_ID} is not available, we look for the old parameter too.
        if (userId == null) {
            userId = request.getParameter(REMOTEAPP_REQUESTOR_ID);
            LOG.debug("2-Legged-OAuth userId [{}] from request parameter [{}].", userId, REMOTEAPP_REQUESTOR_ID);
        }

        // if userId is not null, this is an attempt to do 2LO impersonation.
        if (userId != null) {
            // if 2LO impersonation is not on, the operation is not allowed.
            if (!consumer.getTwoLOImpersonationAllowed()) {
                LOG.info("2-Legged-OAuth with Impersonation request has been attempted but 2-Legged-OAuth with Impersonation is not enabled for consumer:'{}'. Cannot access resource as user '{}'", consumer.getName(), userId);
                sendError(response, HttpServletResponse.SC_UNAUTHORIZED, message);
                return new Authenticator.Result.Failure(new OAuthProblem.PermissionDenied(userId));
            }

            // The user must be a valid user in the system and must be able to both log in.
            // If either of these cases fail, a 401 is returned.
            user = userManager.resolve(userId);
            LOG.debug("2-Legged-OAuth userId [{}] resolved to [{}].", userId, user != null ? user.getName() : "null");
        } else {
            // This is 2LO with no impersonation. We only resolve to the user assigned for execution.
            if (!consumer.getTwoLOAllowed()) {
                LOG.info("2-Legged-OAuth request has been attempted but 2-Legged-OAuth is not enabled for consumer:'{}'.", consumer.getName());
                sendError(response, HttpServletResponse.SC_UNAUTHORIZED, message);
                return new Authenticator.Result.Failure(new OAuthProblem.PermissionDenied());
            }

            // Let's make sure the user is valid.
            if (StringUtils.isBlank(consumer.getExecutingTwoLOUser())) {
                LOG.debug("No executing user assigned for 2LO requests");
                user = null;
            } else {
                LOG.debug("User assigned for 2LO requests is '" + consumer.getExecutingTwoLOUser() + "'");
                user = userManager.resolve(consumer.getExecutingTwoLOUser());
            }
        }

        RequestAnnotations.setOAuthConsumerKey(request, consumer.getKey());
        return getUserLoginResult(request, response, message, consumer, user);
    }

    private Result handleException(HttpServletResponse response, OAuthMessage message, Exception e) {
        // this isn't likely to happen, it would result from some unknown error with the request that the OAuth.net
        // library couldn't handle appropriately
        LOG.error("Failed to validate OAuth message", e);
        sendError(response, HttpServletResponse.SC_INTERNAL_SERVER_ERROR, message);
        return new Result.Error(new OAuthProblem.System(e));
    }

    private Result getUserLoginResult(HttpServletRequest request, HttpServletResponse response, OAuthMessage message, Consumer consumer, Principal user) {
        // if a user is provided, they must exist and be able to login
        if (user != null && !authenticationController.canLogin(user, request)) {
            LOG.info("Access denied because user:'{}' cannot login", user.getName());
            sendError(response, HttpServletResponse.SC_UNAUTHORIZED, message);
            return new Result.Failure(new OAuthProblem.PermissionDenied(user.getName()));
        }

        LOG.info("Authenticated app '{}' as user '{}' successfully", consumer.getKey(), user == null ? "null" : user.getName());
        return new Result.Success(user);
    }

    private Result handleOAuthProblemException(HttpServletResponse response, OAuthMessage message, String tokenStr, OAuthProblemException ope) {
        logOAuthProblem(message, ope, LOG);
        try {
            OAuthServlet.handleException(response, ope, applicationProperties.getBaseUrl());
        } catch (Exception e) {
            // there was an IOE or ServletException, nothing more we can really do
            LOG.error("Failure reporting OAuth error to client", e);
        }

        if (ope.getProblem().equals(CONSUMER_KEY_UNKNOWN)) {
            return new Result.Failure(new OAuthProblem(Problem.valueOf(ope.getProblem().toUpperCase(Locale.ENGLISH))));
        }

        if (tokenStr != null) {
            return new Result.Failure(new OAuthProblem(Problem.valueOf(ope.getProblem().toUpperCase(Locale.ENGLISH)), tokenStr));
        } else {
            return new Result.Failure(new OAuthProblem(Problem.valueOf(ope.getProblem().toUpperCase(Locale.ENGLISH))));
        }
    }

    private ServiceProviderToken getToken(final String tokenStr) {
        return (ServiceProviderToken) transactionTemplate.execute(new TransactionCallback() {
            public Object doInTransaction() {
                return store.get(tokenStr);
            }
        });
    }

    private String getLogicalUri(HttpServletRequest request) {
        String uriPathBeforeForwarding = (String) request.getAttribute(FORWARD_REQUEST_URI);
        if (uriPathBeforeForwarding == null) {
            return null;
        }
        URI newUri = URI.create(request.getRequestURL().toString());
        try {
            return new URI(newUri.getScheme(), newUri.getAuthority(),
                    uriPathBeforeForwarding,
                    newUri.getQuery(),
                    newUri.getFragment()).toString();
        } catch (URISyntaxException e) {
            LOG.warn("forwarded request had invalid original URI path: " + uriPathBeforeForwarding);
            return null;
        }
    }

    private void sendError(HttpServletResponse response, int status, OAuthMessage message) {
        response.setStatus(status);
        try {
            response.addHeader("WWW-Authenticate", message.getAuthorizationHeader(applicationProperties.getBaseUrl()));
        } catch (IOException e) {
            LOG.error("Failure reporting OAuth error to client", e);
        }
    }
}