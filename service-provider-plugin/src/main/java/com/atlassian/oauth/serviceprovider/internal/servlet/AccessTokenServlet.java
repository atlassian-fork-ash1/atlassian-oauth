package com.atlassian.oauth.serviceprovider.internal.servlet;

import com.atlassian.oauth.serviceprovider.Clock;
import com.atlassian.oauth.serviceprovider.InvalidTokenException;
import com.atlassian.oauth.serviceprovider.ServiceProviderToken;
import com.atlassian.oauth.serviceprovider.ServiceProviderToken.Authorization;
import com.atlassian.oauth.serviceprovider.ServiceProviderTokenStore;
import com.atlassian.oauth.serviceprovider.internal.OAuthConverter;
import com.atlassian.oauth.serviceprovider.internal.TokenFactory;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.transaction.TransactionTemplate;
import net.oauth.OAuthMessage;
import net.oauth.OAuthProblemException;
import net.oauth.OAuthValidator;
import net.oauth.server.OAuthServlet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;

import static com.atlassian.oauth.Request.OAUTH_AUTHORIZATION_EXPIRES_IN;
import static com.atlassian.oauth.Request.OAUTH_EXPIRES_IN;
import static com.atlassian.oauth.Request.OAUTH_SESSION_HANDLE;
import static com.atlassian.oauth.serviceprovider.ServiceProviderToken.Version.V_1_0_A;
import static com.atlassian.oauth.serviceprovider.internal.servlet.OAuthProblemUtils.logOAuthProblem;
import static com.google.common.base.Preconditions.checkNotNull;
import static net.oauth.OAuth.OAUTH_TOKEN;
import static net.oauth.OAuth.OAUTH_TOKEN_SECRET;
import static net.oauth.OAuth.OAUTH_VERIFIER;
import static net.oauth.OAuth.Problems.PERMISSION_DENIED;
import static net.oauth.OAuth.Problems.PERMISSION_UNKNOWN;
import static net.oauth.OAuth.Problems.TOKEN_EXPIRED;
import static net.oauth.OAuth.Problems.TOKEN_REJECTED;
import static net.oauth.OAuth.formEncode;
import static net.oauth.OAuth.newList;
import static net.oauth.server.OAuthServlet.handleException;

public class AccessTokenServlet extends TransactionalServlet {
    private final Logger log = LoggerFactory.getLogger(getClass());
    private final TokenFactory factory;
    private final OAuthValidator validator;
    private final ApplicationProperties applicationProperties;
    private final OAuthConverter converter;
    private final ServiceProviderTokenStore tokenStore;
    private final Clock clock;

    public AccessTokenServlet(@Qualifier("tokenStore") ServiceProviderTokenStore tokenStore,
                              TokenFactory factory,
                              OAuthValidator validator,
                              ApplicationProperties applicationProperties,
                              OAuthConverter converter,
                              TransactionTemplate transactionTemplate,
                              Clock clock) {
        super(transactionTemplate);
        this.tokenStore = checkNotNull(tokenStore, "store");
        this.factory = checkNotNull(factory, "factory");
        this.validator = checkNotNull(validator, "validator");
        this.applicationProperties = checkNotNull(applicationProperties, "applicationProperties");
        this.converter = checkNotNull(converter, "converter");
        this.clock = checkNotNull(clock, "clock");
    }

    @Override
    public void doPostInTransaction(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        ServiceProviderToken accessToken;
        try {
            OAuthMessage requestMessage = OAuthServlet.getMessage(request, null);
            requestMessage.requireParameters(OAUTH_TOKEN);
            ServiceProviderToken token;
            try {
                token = tokenStore.get(requestMessage.getToken());
            } catch (InvalidTokenException e) {
                throw new OAuthProblemException(TOKEN_REJECTED);
            }
            if (token == null) {
                throw new OAuthProblemException(TOKEN_REJECTED);
            }
            if (token.isRequestToken()) {
                checkRequestToken(requestMessage, token);
            } else {
                checkAccessToken(requestMessage, token);
            }

            try {
                validator.validateMessage(requestMessage, converter.toOAuthAccessor(token));
            } catch (OAuthProblemException ope) {
                logOAuthProblem(requestMessage, ope, log);
                throw ope;
            }

            accessToken = tokenStore.put(factory.generateAccessToken(token));
            tokenStore.removeAndNotify(token.getToken());
        } catch (Exception e) {
            handleException(response, e, applicationProperties.getBaseUrl(), true);
            return;
        }

        response.setContentType("text/plain");
        OutputStream out = response.getOutputStream();
        formEncode(newList(
                OAUTH_TOKEN, accessToken.getToken(),
                OAUTH_TOKEN_SECRET, accessToken.getTokenSecret(),
                OAUTH_EXPIRES_IN, Long.toString(accessToken.getTimeToLive() / 1000),
                OAUTH_SESSION_HANDLE, accessToken.getSession().getHandle(),
                OAUTH_AUTHORIZATION_EXPIRES_IN, Long.toString(accessToken.getSession().getTimeToLive() / 1000)
        ), out);
    }

    private void checkRequestToken(OAuthMessage requestMessage, ServiceProviderToken token) throws Exception {
        if (token.hasExpired(clock)) {
            throw new OAuthProblemException(TOKEN_EXPIRED);
        }
        if (token.getAuthorization() == Authorization.NONE) {
            throw new OAuthProblemException(PERMISSION_UNKNOWN);
        }
        if (token.getAuthorization() == Authorization.DENIED) {
            throw new OAuthProblemException(PERMISSION_DENIED);
        }
        if (!token.getConsumer().getKey().equals(requestMessage.getConsumerKey())) {
            throw new OAuthProblemException(TOKEN_REJECTED);
        }

        if (V_1_0_A.equals(token.getVersion())) {
            requestMessage.requireParameters(OAUTH_VERIFIER);
            if (!token.getVerifier().equals(requestMessage.getParameter(OAUTH_VERIFIER))) {
                throw new OAuthProblemException(TOKEN_REJECTED);
            }
        }
    }

    private void checkAccessToken(OAuthMessage requestMessage, ServiceProviderToken token) throws Exception {
        if (token.getSession() == null) {
            throw new OAuthProblemException(TOKEN_REJECTED);
        }
        requestMessage.requireParameters(OAUTH_SESSION_HANDLE);
        if (!token.getSession().getHandle().equals(requestMessage.getParameter(OAUTH_SESSION_HANDLE))) {
            throw new OAuthProblemException(TOKEN_REJECTED);
        }
        if (token.getSession().hasExpired(clock)) {
            throw new OAuthProblemException(PERMISSION_DENIED);
        }
    }
}
