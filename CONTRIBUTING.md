Chosing the right repo
======================

This repository has a read only mirror at [bitbucket.org](https://bitbucket.org/atlassian/atlassian-oauth), and an
internal writable repository at [stash.atlassian.com](https://stash.atlassian.com/projects/SJP/repos/atlassian-oauth)

If you are an internal contributor, make sure to create PRs on stash.atlassian.com

If you are an external contributor, fork this repository, and raise the PR on bitbucket.org

Contributing
============

Please see [the platform rules of engagement](https://extranet.atlassian.com/display/KRAK/Server+Platform+Rules+Of+Engagement) for details.

TLDR:
* check if an [issue](https://ecosystem.atlassian.net/projects/OAUTH/issues) exists, or create one
* communicate with the team that you are want to work on the issue, and discuss the scope (either on the issue itself or on Stride - 'Kraken' channel)
* assign the issue to yourself
* create a branch with the name `issue[-<MAJOR>.<MINOR>]/<EAN issue key>-<description>`
* make sure that your change has tests, that the build pases, both locally and on CI
* raise a pull request, wait for approvals
* merge if you have permission, ask the team to merge otherwise
* once merged you should be able to release using [EcoBAC](https://ecosystem-bamboo.internal.atlassian.com/allPlans.action)
* remember to merge the stable branch with your changes forward after the release