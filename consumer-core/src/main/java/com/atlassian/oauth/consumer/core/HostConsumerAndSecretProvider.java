package com.atlassian.oauth.consumer.core;

import com.atlassian.oauth.consumer.core.ConsumerServiceStore.ConsumerAndSecret;

/**
 * Provides the Consumer information for the host application.  The first time the host consumer information is
 * accessed,  the application should generate and store it.  Later accesses should load it from the persistent store,
 * or OAuth tokens created using this information will not work.
 */
public interface HostConsumerAndSecretProvider {
    /**
     * Return the host application {@code ConsumerAndSecret}.  If it has not been created yet, create it and store it.
     *
     * @return host application {@code ConsumerAndSecret}
     */
    ConsumerAndSecret get();

    /**
     * Update the host applications {@code ConsumerAndSecret} information in the persistent store and return the saved
     * value.
     *
     * @param hostCas Value to update the persistent store with
     * @return saved value
     */
    ConsumerAndSecret put(ConsumerAndSecret hostCas);
}
