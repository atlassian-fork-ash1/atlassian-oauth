package com.atlassian.oauth.serviceprovider;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.io.Writer;

/**
 * Provides a way for consumer information to be rendered in the most appropriate way for the consumer, giving the user
 * as much information as they need to make a decision about whether to authorize the request token or not.
 *
 * <p>Note: This is part of the service provider SPI package, but it is not required that applications implement this
 * interface.  A default is provided that is suitable for most usage scenarios.  This is in the SPI so that
 * third-party developers can potentially display more suitable consumer information.  As an example, we can make
 * our service provider OpenSocial aware by creating a plugin which implements this interface and the
 * {@link TokenPropertiesFactory} to pull the gadget spec URI from the request for a request token.  Then, when the
 * user goes to authorize the request token we can display the gadgets information by parsing the gadget spec.
 */
public interface ConsumerInformationRenderer {
    /**
     * Returns {@code true} if this renderer should be used to render information for the consumer making the request,
     * {@code false} otherwise.
     *
     * @param token   the token the consumer wants authorized
     * @param request request made by the consumer
     * @return {@code true} if this renderer should be used to render information for the consumer making the request,
     * {@code false} otherwise
     */
    boolean canRender(ServiceProviderToken token, HttpServletRequest request);

    /**
     * Renders the custom information the renderer knows about the consumer.
     *
     * @param token   request token the consumer wants the user to authorize
     * @param request request from the user to render authorization
     * @param writer  response writer
     * @throws IOException                        thrown if there is a problem writing
     * @throws ConsumerInformationRenderException thrown if there is a non-IO problem while rendering
     */
    void render(ServiceProviderToken token, HttpServletRequest request, Writer writer) throws IOException;
}
